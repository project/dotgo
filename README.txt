// README.txt

dotgo.module
- just the basics are here, currently only support for dotgo "engines" with matches

dotgo_auth.module
- this module lets you authenticate by texting [DOMAIN auth TOKEN] to your appropriate DOTGO short code
- currently supports authenticating only one mobile device at a time, but you can switch back and forth as needed

CREATING a DOTGO module
- first, you need to implement hook_dotgo_engine()
-- that function returns an array of engines, each of which is an array where 'match' => the text command to invoke and 'engine_href' is the path where the engine is
--- note that this can be a full URL to absolutely anywhere, or if it's a drupal path, it will be sent as dotgo/whateveryouput, and whateveryouput is a function
    that your module implements [function whateveryouput($sys_argument)], where sys_argument is what the user texted excluding the domain and command
    It is recommended that whateveryouput be your module name.  For the dotgo_auth.module, the function name is dotgo_auth (engine registered at dotgo/dotgo_auth)
- these callbacks can make use of the API functions for the dotgo module

IF YOU DON'T HAVE CLEAN URL's
- you should go to ?q=index.cmrl
- "view source"
- copy and paste that into a real "index.cmrl" in the root folder of your site
-- remember that when you install new modules this can change, so you may need to re-do it
- alternatively, you can invoke your text message with the first line being the full path to index.cmrl, like yoursite.com?q=index.cmrl